
Single Catchment
===================

.. toctree::
   :maxdepth: 4

   lstm
   cnn_1d
   tcn
   nbeats
   ia_lstm
   fb_profet
   transformer
   informer
   tft
   y_former
   autoformer
   fedformer

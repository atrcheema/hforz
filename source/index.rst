.. ts_hydro_forecast documentation master file, created by
   sphinx-quickstart on Thu Apr 27 09:47:56 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Machine Learning for hydrological forecasting!
===============================================

.. toctree::
   :maxdepth: 4

   notebooks/single_basin/index.rst

